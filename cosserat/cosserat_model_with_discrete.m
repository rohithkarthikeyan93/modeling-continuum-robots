%% Cosserat Rod Model
% Course: MEEN 689 605 Advanced Robotics 
% Author: Rohith Karthikeyan 
% With input from Shivanand Pattanshetti (plotting and debugging)
% Collaborators: Shivanand Pattanshetti, Zaryab Shahid
% Last edit: 04/24/2018 at 7:19 PM
% Comments: Get this to work, check boundary conditions, compare!

clear all
close all
clc
%% Variable Declarations
global in;
in.dO =     1.27 * 10^-3;                                 % outer diameter m
in.dIn =    1.016* 10^-3;                                 % inner diameter m
in.area = pi/4*(in.dO^2 - in.dIn^2);                      % area of cross-section m^2
in.E = 75*10^9;                                           % young's modulus of NiTi N/m^2
in.G = 25*10^9;                                           % some random shear modulus value

in.Ixx = pi/64 * (in.dO^4 - in.dIn^4); 
in.Iyy = pi/64 * (in.dO^4 - in.dIn^4);  
in.Izz = in.Ixx + in.Iyy; 

in.kSE = diag([in.G*in.area in.G*in.area in.G*in.area]);     
in.kBT = diag([in.E*in.Ixx in.E*in.Iyy in.E*in.Izz]);

%% Initialization w/BCs
in.p_0 = [0 0 0]';
in.R_0 = eye(3,3);
in.u_str = [0 0 0]';
in.v_str = [0 0 1]';
in.n_L = [5 -6 -10]';
in.m_L = [10^-9 10^-9 10^-9]';
in.fDist = [0 0 0]';
in.lDist = [0 0 0]';

in.len = 0.1;
in.baseLen = 0.075;
in.midLen = 0.095;
%% Run the solver
mySoln = bvp4c(@findDerivatives, @myBoundaryConditions,bvpinit([0, in.len],...
                zeros(18, 1)),bvpset('NMax', 7000, 'stats', 'on'));
%% Plot Shape
plotShape(mySoln);
%% Function Definitions
function[out] = findDerivatives(s,SV)
global in;
[p,R,n,m] = morphInArray(SV);
[kSE,kBT] = findMechanicalProps(s);
v = kSE\R'*n + in.v_str;
u = kBT\R'*m + in.u_str;
uCap = [ 0, -u(3, 1),  u(2, 1);...
         u(3, 1), 0, -u(1, 1); ...
        -u(2, 1),  u(1, 1),  0 ];
myStruct.pDot = R*v;    
myStruct.RDot = R*uCap;
myStruct.nDot = -in.fDist;
myStruct.mDot = cross(-myStruct.pDot, n) - in.lDist; 
out = morphOutArray(myStruct);
end
function[out] = myBoundaryConditions(y0,yLen) % evaluate residuals
global in;
out = zeros(18, 1);
i = 0;
out(i + 1 : i + 3, 1) = y0(i + 1 : i + 3, 1) - in.p_0;           
i = i + 3;
out(i + 1 : i + 3, 1) = y0(i + 1 : i + 3, 1) - in.R_0(1:3, 1);   
i = i + 3;
out(i + 1 : i + 3, 1) = y0(i + 1 : i + 3, 1) - in.R_0(1:3, 2);   
i = i + 3;
out(i + 1 : i + 3, 1) = y0(i + 1 : i + 3, 1) - in.R_0(1:3, 3);  
i = i + 3;
out(i + 1 : i + 3, 1) = yLen(i + 1 : i + 3, 1) - in.n_L;       
i = i + 3;
out(i + 1 : i + 3, 1) = yLen(i + 1 : i + 3, 1) - in.m_L;     
end
function[p,R,n,m] = morphInArray(in)
i = 0;
    p = in(i + 1 : i + 3, 1);   
    i = i + 3;
    for j = 1:3
         R(1:3, j) =  in(i + 1 : i + 3, 1);  
         i = i + 3;
    end
   n         =  in(i + 1 : i + 3, 1);   
   i = i + 3;
   m         =  in(i + 1 : i + 3, 1); 
end
function[out] = morphOutArray(s)
i = 0;
out(i + 1 : i + 3, 1) = s.pDot;           
i = i + 3;
for j = 1:3
   out(i+1:i+3, 1) =  s.RDot(1:3,j);  
   i = i + 3;
end
out(i + 1 : i + 3, 1) = s.nDot;           
i = i + 3;
out(i + 1 : i + 3, 1) = s.mDot;  
end
function[] = plotShape(mySoln)
global in;
s = 0:(in.len/500):in.len;
    diffSolns = deval(mySoln, s);
    myCords = zeros(4, length(s));
    index = 0;
    p         = diffSolns(index + 1 : index + 3, :);   index = index + 3;
    R(1:3, :) = diffSolns(index + 1 : index + 3, :);   index = index + 3;
    R(4:6, :) = diffSolns(index + 1 : index + 3, :);   index = index + 3;
    R(7:9, :) = diffSolns(index + 1 : index + 3, :);
    or = [0 0 0 1]';
    dummy = zeros(1,3);
    myT = [in.R_0 in.p_0; dummy 1];
    
    for i=1:length(s)
        RNew(1:3, 1) = R(1:3, i);
        RNew(1:3, 2) = R(4:6, i);
        RNew(1:3, 3) = R(7:9, i);
        myCords(:, i) = myT*or;
        
        % The next transformation matrix
        myT = [...
                      RNew, p(:, i); ...
            zeros(1, 3),       1  ...
        ];
    end
    
    x = myCords(1, :);
    y = myCords(2, :);
    z = myCords(3, :);
        
        
figure              % Create new figure
pbaspect([1 1 0.5]) % Control the aspect ratio of the axes (plot box)
daspect([1 1 1])    % Control the aspect ratio of the data in the plot
plot3(x, y, z);     % The actual plotting
xlabel('X');
ylabel('Y');
zlabel('Z');
grid on
box on
grid minor
xlim([-0.1, 0.1]);
ylim([-0.1, 0.1]);
zlim([0, 0.1]);

        
end
function [kSE,kBT] = findMechanicalProps(s)
global in;
if s <= in.baseLen
    kSE = in.kSE;
    kBT = in.kBT;
elseif (s > in.baseLen && s<=in.midLen)
    kSE = 1.25*in.kSE;
    kBT = 1.25*in.kBT;
else 
    kSE = 0.25*in.kSE;
    kBT = 0.25*in.kBT;
end
end