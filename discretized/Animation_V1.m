

%% Animate Plot
 manipulatorShape = animatedline;
 tipTrajectory = animatedline;
  
 %% animate this stuff  
setGraphicProperties(manipulatorShape, tipTrajectory)

xlabel('Position - X (cm)');
ylabel('Position - Y (cm)');
title('Real-time plot of bot and map');
box on;
grid on;

for i =1:length(inDataSets) %inDataSets - refers to number of configs of your manipulator
addpoints(manipulatorShape,X,Y,Z)
drawnow
end

 function[] = setGraphicProperties(X,Y)

X.LineStyle = '--';         
Y.LineStyle = 'none';    

X.Color =   'red';
Y.Color =   'red';

X.Marker =  'o';
Y.Marker =  '.';

X.MarkerSize = 10;
Y.MarkerSize = 20;

%axis([-100,200,-100,200]);                              
 end
 