%% Smart Needle: Rigid Link Model
% To simulate the deformation behavior of a continuum robot (Smart Needle)
% Under external load and moment
% Using serial rigid link approximation
% Author: Rohith Karthikeyan - Last Saved: 11.39 PM 5/29/2018 (CDT)
%% Continuum Robot Deformed Shape Simulation
clc
clear all
close all
%% Variable Declarations - segment by type
n = 4;                                  %Number of Links
jointArgs = sym('jointVar',[1 2*n]);    %Joint space variables
psi = jointArgs(1:n);                   %symbolic array for psi
theta = jointArgs(n+1:2*n);             %symbolic array for theta

ls = 5;                                 %Joint separation
I = pi/2*((1.2446/2)^4-(1.016/2)^4);    %Tube area moment of intertia
E = 83*10^3;                            %Young's Modulus of NiTi
k = E*I/ls;                             %Stiffness Value
F =  [0 0 0];                           %Tip Load
M1 = [0 0 100];                           %Mid-way moment
M2 = [0 0 0];                           %End moment

nM1 = 3;                                %Position of Mom1
nM2 = 4;                                %Position of Mom2
nF1 = 4;                                %Position of F1

%% For Comparison

% Solid NiTI EI = 83 GPa(E)
% OD = 1.2446 mm
% ID = 1.016 mm
% Proximal Pattern 0.5*(E)
% Joint = 0.1*(E)
% Tip Force [1 0 0] and [0 1 0] base coordinates (2 cases)
% Moment: [0.01 0 0] Nm,  [0 0.01 0] Nm

% // check position vector definitions - derived by hand
% // check transoformation formula - checked and corrected
% // check psi definition
% // read paper - check main moment derivation and definitions! - mainLoop fix
% // correct position vectors pad with ones!
% // check fsolve setup
% // check graphical representation
% // Why are values not being solved for?
% //Check consistence of dimensions

% Ensure correctness of transforms
% Where is the system origin?


% IMPORTANT: Correct system origin position - work backwards from jt cord
% of 2?
% Origin frame not oriented the same way as caresian frames?
% Check definition of jtCords

% comment and beautify
% change manipulator definition: Pattern_tube_flexure_tube? (not needed)

%% Main Loop - Solve My Eqns
for i = 1:n 
    % Assuming moments and forces are defined wrt some local coordinates
    if nM1 < i
        rM1 = zeros(4,1)+[0 0 0 1]';
    else
        % T1 =  findTransMatrix(1,nM1,theta,psi,ls); % define all wrt base frame (1)
        % rM1 = T1*[M1 1]';
        rM1 = [M1 1]';
    end
    
    if nM2 <i
       rM2 = zeros(4,1)+[0 0 0 1]';
    else
        % T2 = findTransMatrix(1,nM2,theta,psi,ls); % define all wrt base frame (1)
        % rM2 = T2*[M2 1]';
        rM2 = [M2 1]';
    end
     
        if nF1 <i
        rF1 = zeros(4,1)+[0 0 0 1]';
        else
        T3 = findTransMatrix(1,nF1,theta,psi,ls);
         if(i==1)
             posV = (T3)*[0 0 0 1]';
         else
              crL = 2*(ls/(theta(i-1)))*sin(theta(i-1)/2);   % cord length
             posV = (T3)*[-crL*sin(theta(i-1)/2) 0 crL*cos(theta(i-1)/2)  1]';
         end
        rF1 = cross(posV(1:3,:),F);
        rF1 = [rF1 1]'; 
        end
    
bm{i} = rM1+rM2+rF1 - [0 0 0 2]'; % get rid of additional '1s'
vectorMagnitude = norm(bm{i}); 
jointAngles(i)= vectorMagnitude/k;
bendingDirection(i) = acos(bm{i}(2)/vectorMagnitude); % Angle wrt to 'y' direction - is this correct?
end

myEqns = fsolveSetup(jointAngles, bendingDirection, psi(1:end), theta(1:end));
jointVals= solve(myEqns,jointArgs);

fields = fieldnames(jointVals);
for i = 1:numel(fields)
    jV(i) = jointVals.(fields{i});                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         jointVals.(fields{i})
end
theVals = jV(1:n);
psiVals = jV(n+1:2*n);


%% Collect data for plots - check this!
for i = 1:n
    if i==1
        jtCords{i} = [0,0,0,1]';
    elseif i==2 
           crL = 2*(ls/(theVals(i-1)))*sin(theVals(i-1)/2);
           jtCords{i} = [-crL*sin(theVals(i-1)/2) 0 crL*cos(theVals(i-1)/2)  1]';
    else
        crL = 2*(ls/(theVals(i-1)))*sin(theVals(i-1)/2);   % cord length
        jtCords{i} = findTransMatrix(1,i,psiVals,theVals,ls)*[-crL*sin(theVals(i-1)/2) 0 crL*cos(theVals(i-1)/2)  1]'; % Why i-1?
    end       
end
%% Do some plotting
for i = 1:length(jtCords)
hold on
xData(i) = jtCords{i}(1);
yData(i) = jtCords{i}(2);
zData(i) = jtCords{i}(3);
scatter3(jtCords{i}(1),jtCords{i}(2),jtCords{i}(3));
xlim([-50 50]);
ylim([-50 50]);
zlim([-50 50]);
end

myData = [xData' yData' zData'];
line(xData,yData,zData);
grid on
box on

%% Add circular arcs between points
for i =1:length(jtCords)
pts{i} = double([jtCords{i}(1) jtCords{i}(3)]);
end
for i =1:length(pts)-1
syms x y
syms X Y
r = ls/theVals(i);
[x,y]=solve((x-pts{i}(1))^2+(y-pts{i}(2))^2==r^2,(x-pts{i+1}(1))^2+(y-pts{i+1}(2))^2==r^2,x,y);
ezplot((X-x(1))^2+(Y-y(1))^2==r^2,[min(pts{i}(1),pts{i+1}(1)),max(pts{i}(1),pts{i+1}(1)), ...
    min(pts{i}(2),pts{i+1}(2)),max(pts{i}(2),pts{i+1}(2))])
end

%% Fsolve setup function
function F = fsolveSetup(jointAngles, bendingDirection, psi, theta)
F = [jointAngles'-theta';
    bendingDirection'-psi'];
end
%% Find transformation between 2 frames from n2-1 to n1
function [tMat] = findTransMatrix(n1,n2,theta,psi,ls)
count=0;
tMatOld = eye(4);
if n1==n2 %this is wrong - fix it
tMat = eye(4);
else
for i = n1:n2-1
rMat = rSymZ(psi(n2-1-count))*rSymY(theta(n2-1-count))*rSymZ(-psi(n2-1-count)); %check index variables here
crL = 2*(ls/(theta(n2-1-count)))*sin(theta(n2-1-count)/2);
posVector = [0 0 0 -crL*sin(theta(n2-1-count)/2); 0 0 0 0; 0 0 0 crL*cos(theta(n2-1-count)/2);0 0 0 0];
tMat = (rMat+posVector)*tMatOld;
tMatOld = tMat;
count= count+1;
end
end
end

%% Symbolic Rotation Matrices: X, Y, and Z
function[rX] = rSymX(t)
rX = [1 0 0 0; 0 cos(t) -sin(t) 0; 0 sin(t) cos(t) 0; 0 0 0 1];
end
function[rY] = rSymY(t)
rY = [1 0 0 0; 0 cos(t) -sin(t) 0; 0 sin(t) cos(t) 0;  0 0 0 1];
end
function[rZ] = rSymZ(t)
rZ = [1 0 0 0; 0 cos(t) -sin(t) 0; 0 sin(t) cos(t) 0; 0 0 0 1];
end