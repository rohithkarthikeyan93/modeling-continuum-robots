%% Exam 1 - HW 2 - MEEN 689 Advanced Robotics
%{
Version 2
Name: Rohith Karthikeyan
Last edit: 03/31/2018 @ 19:29
Comment: This works
%}
clc
clear all
close all
%% Variable declarations
delT = 1/200;                               % Update rate of controller
tNet = 10;                                  % Time to target
qOld = [0 0 0]';                            % Starting configuration
xDes = [0 1.5];                             % Desired target (task-space)
lnkLt = [1 0.5 0.5];                        % Link lengths
eeStrt = [lnkLt(1)+lnkLt(2)+lnkLt(3) 0];    % End-effector starting position
xDotDes = (xDes - eeStrt)/tNet;             % Desired end-effector velocity
count = 0;                                  % Loop counter

%% Main Loop
for i = 0:delT:tNet %check if '0' start is an issue
    count = count+1;
    currentJacob = findJacob(qOld,lnkLt);       % Current jacobian
    srJacobInv = srJInv(currentJacob,0.05);     % Current SRJ-inverse
    qDotOld = srJacobInv*xDotDes';              % Current joint velocities
    qCurrent{count,1} = qOld + delT.*qDotOld;   % Current joint positions
    [eePos{count,1} j1Pos{count,1} j2Pos{count,1}]  = doFKin(qCurrent{count,1},lnkLt);
    qOld = qCurrent{count,1};
end
%% Plot stuff
h=figure
ee = animatedline;
jts = animatedline;
filename = 'No_obstacle_singularityRobust.gif';
box on
grid on
hold on
xlim([-0.5 2.25])
ylim([-0.5 2.25])
xlabel('X1')
ylabel('X2')
setGraphicProperties(ee,jts);
addpoints(jts,0,0);
desiredPath = plot(eeStrt,xDes,'--');
target = plot(xDes(1),xDes(2),'xk','MarkerSize',8);
pbaspect([1 1 1])
for i = 1:100:length(eePos)  
mytime= floor(i*delT);
title(['Robot motion profile | Time: ' num2str(mytime) ' s'])
addpoints(ee,eePos{i}(1),eePos{i}(2));
addpoints(jts,j1Pos{i}(1),j1Pos{i}(2));
addpoints(jts,j2Pos{i}(1),j2Pos{i}(2));
X = [0 j1Pos{i}(1) j2Pos{i}(1) eePos{i}(1)]';
Y = [0 j1Pos{i}(2) j2Pos{i}(2) eePos{i}(2)]';
maniPose = plot(X,Y,'--','Color',[0.8,0.6,0]);
legend([desiredPath maniPose target ee jts],{'Desired trajectory','Robot pose', 'Target position' 'End-effector' 'Intermediate joints'});
drawnow
pause(0.001)

% Capture the plot as an image 
      frame = getframe(h); 
      im = frame2im(frame); 
      [imind,cm] = rgb2ind(im,256); 
      % Write to the GIF File 
      if i == 1 
          imwrite(imind,cm,filename,'gif', 'Loopcount',inf); 
      else 
          imwrite(imind,cm,filename,'gif','WriteMode','append'); 
      end 
end
%% Forward Kinematics
%{ 
Find EE position (eePos) given joint variables (jtVars)
and link dimensions (L) as arrays for the given configuration
%}
function[eePos,j1Pos,j2Pos] = doFKin(jtVars,L)
eePos = [L(1)*cos(jtVars(1))+L(2)*cos(jtVars(1)...
    +jtVars(2))+L(3)*cos(jtVars(1)+jtVars(2)+jtVars(3));... 
       L(1)*sin(jtVars(1))+L(2)*sin(jtVars(1)...
    +jtVars(2))+L(3)*sin(jtVars(1)+jtVars(2)+jtVars(3))];
j2Pos = [L(1)*cos(jtVars(1))+L(2)*cos(jtVars(1)...
    +jtVars(2));... 
       L(1)*sin(jtVars(1))+L(2)*sin(jtVars(1)...
    +jtVars(2))];
j1Pos = [L(1)*cos(jtVars(1));... 
       L(1)*sin(jtVars(1))];
end
%% Jacobian Matrix
%{ 
Find jacobian (myJacob) given joint variables (jtVars)
and link dimensions (L) as arrays for the given configuration
%}
function [myJacob] = findJacob(jtVars,L)
j1 = [ -L(1)*sin(jtVars(1))-L(2)*sin(jtVars(1)...
    +jtVars(2))-L(3)*sin(jtVars(1)+jtVars(2)+jtVars(3));...
    L(1)*cos(jtVars(1))+L(2)*cos(jtVars(1)...
    +jtVars(2))+L(3)*cos(jtVars(1)+jtVars(2)+jtVars(3))];

j2 = [ -L(2)*sin(jtVars(1)...
    +jtVars(2))-L(3)*sin(jtVars(1)+jtVars(2)+jtVars(3));...
    L(2)*cos(jtVars(1)...
    +jtVars(2))+L(3)*cos(jtVars(1)+jtVars(2)+jtVars(3))];

j3 = [ -L(3)*sin(jtVars(1)+jtVars(2)+jtVars(3));...
  L(3)*cos(jtVars(1)+jtVars(2)+jtVars(3))];

myJacob = [j1 j2 j3];
end
%% Singularity Robust Jacobian Inverse
%{ 
Find singularity robust jacobian inverse (invJ) given jacobian (inJ)
and lambda term for the given configuration
%}
function[invJ] = srJInv(inJ,lambda)
n = size(inJ,1);
I = eye(n,n);
invJ = inJ'*((inJ*inJ'+(lambda^2*I))^-1);
end
%% Set Graphic Properties
%{ 
Set graphical properties for animated line features - not optimized
%}
 function[] = setGraphicProperties(X,Y)
X.LineStyle = 'none';         
X.Marker =  'o';
X.MarkerSize = 5;
X.MarkerEdgeColor = 'black';
X.MarkerFaceColor = 'red';

Y.LineStyle = 'none';
Y.Color = 'blue';
Y.Marker =  'o';
Y.MarkerSize = 3.5;
Y.MarkerEdgeColor = 'black';
Y.MarkerFaceColor = 'green';

 end
