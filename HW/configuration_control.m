%% Exam 1 - HW 2 - MEEN 689 Advanced Robotics
%{
Version 8 - ConfigControl
Name: Rohith Karthikeyan
Last Edit: 03/31/2018 @ 19:36
Notes: Should work
%}
clc
clear all
close all
%% Variable declarations
delT = 1/200;                               % Update rate of controller
tNet = 10;                                  % Time to target
qOld = [0 0 0]';                            % Starting configuration
xDes = [0 1.5]';                             % Desired target (task-space)
lnkLt = [1 0.5 0.5];                        % Link lengths
eeStrt = [lnkLt(1)+lnkLt(2)+lnkLt(3) 0]';    % End-effector starting position
xDotDes = (xDes - eeStrt)/tNet;             % Desired end-effector velocity
count = 0;                                  % Loop counter
origin = [0 0]';                            % Origin of Manipulator
objPos = [0 1]';                            % Circular Object Location
radObj = 0.6;                               % Circular Object Radius
%% Main Loop
for i = 0:delT:tNet %check if '0' start is an issue
   
   count = count+1;
   jC = findJacob(qOld,lnkLt,'ee');            % Current jacobian for end-effector
   [eePos,j1Pos,j2Pos] = doFKin(qOld,lnkLt);   % Find cartesian coordinates for all joints
   jtCords = [origin j1Pos j2Pos eePos];
   
   for j= 1:length(jtCords)-1
   [dmin(j), c{j}, t0(j)] = objLnkDist(jtCords(:,j),jtCords(:,j+1),objPos);
   end
   
   [myMin,indexMin] = min(dmin);
   xCP = c{indexMin};
   xRef = jtCords(:,indexMin);
   xCPPos = norm(xCP - xRef);
  
   switch indexMin
       case 1
            ref = 'l1';
            lCP = xCPPos;
            nJcp  =1;
       case 2
            ref = 'l2';
            lCP = [lnkLt(1) xCPPos];
            nJcp  =2;
       case 3
            ref = 'l3';
            lCP = [lnkLt(1) lnkLt(2)  xCPPos];
            nJcp  =3;
   end
   
   if myMin-radObj > 0 % consider threshold w/radObj
       Jcp = zeros(2,nJcp);
   else
       Jcp = findJacob(qOld(1:indexMin),lCP,ref);
   end
   % ensure that Jcp has the same number of columns as Je
   colCount = size(Jcp,2);
   jCP_padded = Jcp;
   if colCount < size(jC,2)
       for k = 1:(size(jC,2)-colCount)
      jCP_padded = [jCP_padded zeros(2,1)];
       end
   end 
   zDotDes = avoidObjVel(myMin,objPos,xCP,jCP_padded,qOld);
   zDotDes = zDotDes';
   qDotOld = doConfigCtrl(jC,30,xDotDes,jCP_padded,30,zDotDes,0.3);
   qCurrent{count,1} = qOld + delT.*qDotOld;  
   [eeP{count,1} j1P{count,1} j2P{count,1}]  = doFKin(qCurrent{count,1}(:,1),lnkLt);
   qOld = qCurrent{count,1};    
end
%% Plot stuff
ee = animatedline;
jts = animatedline;
box on
grid on
hold on
xlim([-0.5 2.25])
ylim([-0.5 2.25])
setGraphicProperties(ee,jts);
addpoints(jts,0,0);
desiredPath = plot(eeStrt,xDes,'--');
target = plot(xDes(1),xDes(2),'xk','MarkerSize',8);
xlabel('X1')
ylabel('X2')
pbaspect([1 1 1])
for i = 1:100:length(eeP)
mytime= floor(i*delT);
title(['Robot motion profile | Time: ' num2str(mytime) ' s'])
addpoints(ee,eeP{i}(1),eeP{i}(2));
addpoints(jts,j1P{i}(1),j1P{i}(2));
addpoints(jts,j2P{i}(1),j2P{i}(2));
X = [0 j1P{i}(1) j2P{i}(1) eeP{i}(1)]';
Y = [0 j1P{i}(2) j2P{i}(2) eeP{i}(2)]';
maniPose = plot(X,Y,'--','Color',[0.8,0.6,0]);
objCircle = viscircles(objPos',0.2);
soiCircle = viscircles(objPos',radObj,'LineStyle','--','Color','b');
legend([desiredPath maniPose target ee jts objCircle soiCircle],{'Desired trajectory','Robot pose', 'Target position' 'End-effector' 'Intermediate joints' 'Obstacle' 'Contact zone'});

drawnow
pause(0.001)
end
%% Forward Kinematics
%{ 
Find EE position (eePos) given joint variables (jtVars)
and link dimensions (L) as arrays for the given configuration
%}
function[eePos,j1Pos,j2Pos] = doFKin(jtVars,L)
eePos = [L(1)*cos(jtVars(1))+L(2)*cos(jtVars(1)...
    +jtVars(2))+L(3)*cos(jtVars(1)+jtVars(2)+jtVars(3));... 
       L(1)*sin(jtVars(1))+L(2)*sin(jtVars(1)...
    +jtVars(2))+L(3)*sin(jtVars(1)+jtVars(2)+jtVars(3))];
j2Pos = [L(1)*cos(jtVars(1))+L(2)*cos(jtVars(1)...
    +jtVars(2));... 
       L(1)*sin(jtVars(1))+L(2)*sin(jtVars(1)...
    +jtVars(2))];
j1Pos = [L(1)*cos(jtVars(1));... 
       L(1)*sin(jtVars(1))];
end
%% Jacobian Matrix
%{ 
Find jacobian (myJacob) given joint variables (jtVars)
and link dimensions (L) as arrays for the given configuration
%}
function [myJacob] = findJacob(jtVars,L,pointRef)
switch pointRef
     case 'ee'
j1 = [ -L(1)*sin(jtVars(1))-L(2)*sin(jtVars(1)...
    +jtVars(2))-L(3)*sin(jtVars(1)+jtVars(2)+jtVars(3));...
    L(1)*cos(jtVars(1))+L(2)*cos(jtVars(1)...
    +jtVars(2))+L(3)*cos(jtVars(1)+jtVars(2)+jtVars(3))];

j2 = [ -L(2)*sin(jtVars(1)...
    +jtVars(2))-L(3)*sin(jtVars(1)+jtVars(2)+jtVars(3));...
    L(2)*cos(jtVars(1)...
    +jtVars(2))+L(3)*cos(jtVars(1)+jtVars(2)+jtVars(3))];

j3 = [ -L(3)*sin(jtVars(1)+jtVars(2)+jtVars(3));...
  L(3)*cos(jtVars(1)+jtVars(2)+jtVars(3))];

myJacob = [j1 j2 j3];

    case 'l1'
       
myJacob = [ -L(1)*sin(jtVars(1));...
    L(1)*cos(jtVars(1))];

    case 'l2'
        
        j1 = [ -L(1)*sin(jtVars(1))-L(2)*sin(jtVars(1)...
    +jtVars(2));...
    L(1)*cos(jtVars(1))+L(2)*cos(jtVars(1)...
    +jtVars(2))];

j2 = [ -L(2)*sin(jtVars(1)...
    +jtVars(2));...
    L(2)*cos(jtVars(1)...
    +jtVars(2))];


myJacob = [j1 j2];
        
    case 'l3'
        j1 = [ -L(1)*sin(jtVars(1))-L(2)*sin(jtVars(1)...
    +jtVars(2))-L(3)*sin(jtVars(1)+jtVars(2)+jtVars(3));...
    L(1)*cos(jtVars(1))+L(2)*cos(jtVars(1)...
    +jtVars(2))+L(3)*cos(jtVars(1)+jtVars(2)+jtVars(3))];

    j2 = [ -L(2)*sin(jtVars(1)...
    +jtVars(2))-L(3)*sin(jtVars(1)+jtVars(2)+jtVars(3));...
    L(2)*cos(jtVars(1)...
    +jtVars(2))+L(3)*cos(jtVars(1)+jtVars(2)+jtVars(3))];

j3 = [ -L(3)*sin(jtVars(1)+jtVars(2)+jtVars(3));...
  L(3)*cos(jtVars(1)+jtVars(2)+jtVars(3))];

myJacob = [j1 j2 j3];
        
end
end
%% Singularity Robust Jacobian Inverse
%{ 
Find singularity robust jacobian inverse (invJ) given jacobian (inJ)
and lambda term for the given configuration
%}
function[invJ] = srJInv(inJ,lambda)
n = size(inJ,1);
I = eye(n,n);
invJ = inJ'*((inJ*inJ'+(lambda^2*I))^-1);
end
%% Set Graphic Properties
 function[] = setGraphicProperties(X,Y)
X.LineStyle = 'none';         
X.Marker =  'o';
X.MarkerSize = 5;
X.MarkerEdgeColor = 'black';
X.MarkerFaceColor = 'red';

Y.LineStyle = 'none';
Y.Color = 'blue';
Y.Marker =  'o';
Y.MarkerSize = 3.5;
Y.MarkerEdgeColor = 'black';
Y.MarkerFaceColor = 'green';
 end
%% Shortest Distance Between Point and Line
function [d, C, t0] = objLnkDist(A, B, P, varargin)
% - number of inputs
narginchk(3, 4);
% - points defined as vectors
if ~(isvector(A) && isvector(B) && isvector(P))
    error('objLnkDist:InvalidVectorInput', ...
            'Points A, B and P should be defined as vectors.');
end
% - points all defined in N-dimensional space
N = length(A); 
if ~(length(B) == N && length(P) == N)
    error('objLnkDist:VectorLengthMismatch', ...
            'A, B and P should be vectors of the same length.');
end
% - at least a 2D problem
if N < 2
     error('objLnkDist:VectorLengthLimit', ...
            'Vectors A, B and P must have 2 or more elements.');
end
% Algorithm
% Direction vector 
M = B - A;
% Running parameter t0 defines the intersection point of line through A and B
% and the perpendicular through P
t0  = dot(M, P - A) / dot(M, M);
% Intersection point of the perpendicular and line through A and B
intersectPnt = A + t0 * M;
        if t0 < 0
            % Start point is closest.
            C   = A;
        elseif t0 > 1
            % End point is closest.
            C   = B;
        else
            % Intersection point is closest.
            C   = intersectPnt;
        end     
% Distance between independent point and closest point
d   = norm(P-C);
end
%% Secondary Task Velocity
function [zDotDes] = avoidObjVel(dmin,xobj,xcp,Jcp,qDotCurrent)
u = (xcp-xobj)'/dmin;
zDotDes = -u*Jcp*qDotCurrent; 
end
%% Configuration Control  
function [qDotOut] = doConfigCtrl(Jc,wcval,xDotDes,Je,weval,zDotDes,wv_val)
n1 = size(Jc,1);
n2 = size(Je,1);
n3 = size(Jc,2);
Wc = wcval*eye(n1,n1);
We = weval*eye(n2,n2); 
Wv = wv_val*eye(n3,n3);
qDotOut = (Jc'*Wc*Jc+Je'*We*Je+Wv)^-1*(Jc'*Wc*xDotDes+Je'*We*zDotDes);
end




